# ChatX -- Technical Test Vendredi

## Statement
ChatX is a simple chat webapp you should write with modern web technologies.
Challenge
You will build ChatX as a real time chat app. 

We provide 2 levels of difficulty for this challenge. Depending on what we agreed on, we will expect you to build only level 1, or both levels; however you are free to do more or less and in accordance with the time you want to dedicate to the challenge.
The usual completion time for this challenge is 2 hours. We recommend to not spend more than 3 hours on it.

Level 1: single chat room
There is only 1 chat room where users can discuss:
- User can read the last 10 messages of the room when entering
- User can read new messages from other users
- User can write and send new messages

Level 2: multiple chat rooms
We now want to have multiple chat rooms.
In addition to the user stories of level 1:
- User can see a list of available chat rooms
- User can join and leave a chatroom
- BONUS: User can see the list of users in a chat room
- BONUS: User can create a chat room by entering a name

Expectations
You can start from scratch or use any boilerplate you wish (of course real time chat boilerplates are not expected).

The recommended technologies are:
- Laravel 7 +
- SQLite DB
- Vue.JS 2+

## Author
Yannick Guern <pro@guern.eu>

## Requirements
- PHP 7.4
- composer
- Docker ( with Laradock )
- git ( with Laradock 
- yarn

## How to run

First copy the env file

```cp config/chatx.env chatx/.env```

### With Laradock
If you already have PHP/composer/yarn environment, you can skip the following lines

Laradock is a project which helps to quickly scaffold a laravel project it comes with a full nginx/php-fpm stack + a workspace with npm/composer already installed

To be able to use it you have to install Laradock

First launch `git submodule init`

Then `git submodule update`

This will clone into the `laradock` folder all you need and much more.

Then you need to copy laradock `.env` file with

```cp config/laradock.env laradock/.env```

When this done you can launch 

```cd laradock && docker-compose up -d nginx```

This will spinup a laravel server through nginx

To enter in your container

```docker-compose exec workspace bash```

### Bootstrap the application
First install PHP libraries

```composer install --no-dev```

Then generate APP key

```php artisan key:generate --ansi```

Then create a sqlite database 

```touch database/database.sqlite```

Then migrate the model

```php artisan migrate```

If you use Laradock all is good, go to `http://localhost`

If not run `cd  chatX && php artisan serve` you can specify a custom port if you want.


## Time partition
- Bootstrap project (laradock, laravel ui, hot-reload, database, etc...) : 1h30
- Writing Readme and testing How to : 30 min
- Sketching : 1h
- Development: 7h 

## What did I achieve
- Design of the application with [Figma](https://www.figma.com/file/PpXw98HOEyyQ2BOQzUMonT/chatX?node-id=0%3A1)
- Modeling of the database with MysqlWorbench into `/design/model`
- Migration files
- The store with all interactions with Laravel API
- The Laravel API mostly with resource controllers

Taking about features:
- A user can connect to the system with only its name
- A user can join or create a chat room
- A user can send a message and see the ten last messages of the chat room
- A user can see the list participants
- The system can handle as many chat room as we want to

## What I didn't
- It's not a real time chat, I poll the API every 5s, I run out of time to use Laravel messages and queues to finish that
- If user shutdown the tab or the browser, it will be a chat room participant forever
- I don't implement a job process to clean all users not related to any room
- Unit test could be further
- I don't create CI to check my tests