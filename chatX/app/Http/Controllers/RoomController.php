<?php

namespace App\Http\Controllers;

use App\Room;
use App\Rooms\Requests\CreateMessageRequest;
use App\Rooms\Requests\CreateRoomRequest;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return string
     */
    public function index()
    {
        return Room::all()->toJson();
    }

    public function messages(Request $request, $uuid)
    {
        $room = Room::find($uuid);
        if (!$room) {
            return [];
        }
        return $room->messages();
    }

    public function users(Request $request, $uuid)
    {
        $room = Room::find($uuid);
        if (!$room) {
            return [];
        }
        return $room->users();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRoomRequest $request)
    {
        $data = $request->all();
        $data['uuid'] = Uuid::uuid4()->toString();
        return Room::create($data, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
