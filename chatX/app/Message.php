<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $primaryKey = 'uuid';
    public $keyType = 'string';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'author', 'content', 'room_uuid'
    ];

    public function user() {
        return $this->hasOne(User::class, 'uuid', 'author');
    }
}
