<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $primaryKey = 'uuid';
    public $keyType = 'string';
    public $incrementing = false;
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'uuid', 'name'
    ];

    public function messages() {
      return $this->hasMany(Message::class, 'room_uuid', 'uuid')
          ->orderBy('created_at', 'desc')
          ->take(10)->get()->map(function (Message $message){
              $data = $message->toArray();
              $data['author'] = $message->user->name;
              return $data;
          });
    }

    public function users() {
        return $this->hasMany(User::class, 'room_uuid', 'uuid')->get();
    }
}
