import {STEP_CHOOSE_ROOM, STEP_CONNECTION, STEP_CREATE_ROOM, STEP_IN_ROOM} from "./mutations";

export const GET_UPPER_CONTAINER_TEXT = 'GET_UPPER_CONTAINER_TEXT';
export const GET_MIDDLE_CONTAINER_FORM = 'GET_MIDDLE_CONTAINER_FORM';
export const GET_LAYOUT = 'GET_LAYOUT';

export default {
    [GET_UPPER_CONTAINER_TEXT]: (state) => {
        switch (state.step) {
            case STEP_CONNECTION:
                return "Entres ton nom et rejoins un des nombreux salons de discussions disponibles.";
            case STEP_CHOOSE_ROOM:
                return "Les discussions sont séparées en salons, libre à toi d’en rejoindre d’en créer un nouveau.";
            case STEP_CREATE_ROOM:
                return "Les salons de discussions sont publics, tout le monde peut y entrer et ajouter un message.";
            default:
                return "Une erreur s'est produite";

        }
    },
    [GET_MIDDLE_CONTAINER_FORM]: (state) => {
        switch (state.step) {
            case STEP_CONNECTION:
                return "FormCreateUser";
            case STEP_CHOOSE_ROOM:
                return "FormListRooms";
            case STEP_CREATE_ROOM:
                return "FormCreateRoom";
            default:
                return "";

        }
    },
    [GET_LAYOUT]: (state) => {
        switch (state.step) {
            case STEP_CONNECTION:
            case STEP_CHOOSE_ROOM:
            case STEP_CREATE_ROOM:
                return "DefaultLayout";
            case STEP_IN_ROOM:
                return "RoomLayout"
            default:
                return "";

        }
    }
}
