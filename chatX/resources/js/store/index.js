import mutations, {STEP_CHOOSE_ROOM, STEP_CONNECTION, STEP_IN_ROOM} from "./mutations";
import getters from "./getters";
import user from './modules/user';
import rooms from './modules/rooms';
import messages from './modules/messages';

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
    modules: {user, rooms, messages},
    strict: debug,
    mutations,
    getters,
    state: {
        collapse: true,
        roomUsers: false,
        step: STEP_CONNECTION
    },
    namespaced: true
})
