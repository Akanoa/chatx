import {ADD_MESSAGE} from "./mutations";

export const GET_MESSAGES = 'GET_MESSAGES';
export const SEND_MESSAGE = 'SEND_MESSAGE';

export default {
    [SEND_MESSAGE]: ({commit, rootState}, {content}) => {

        return new Promise(resolve => {
            fetch('/api/messages', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    content,
                    room_uuid: rootState.rooms.current_room_uuid,
                    author: rootState.user.uuid
                })
            }).then(response => {
                if (!response.ok) {
                    throw response
                }
                return response.json();
            }).then(data => {
                commit(ADD_MESSAGE, data)
                resolve()
            })
        });
    },
    [GET_MESSAGES]: ({commit, rootState}) => {
        let room = rootState.rooms.current_room_uuid;
        fetch('/api/rooms/'+room+'/messages')
            .then(response => {
                if (!response.ok) {
                    throw response
                }
                return response.json()
            }).then(data => {
                for (let message of data.reverse()) {
                    commit(ADD_MESSAGE, message)
                }
            })
    }
}
