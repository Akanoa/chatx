import mutations from "./mutations";
import actions from "./actions";

export default {
    state: {
        messages: [],
        uuids: []
    },
    mutations,
    actions,
    namespaced: true
}
