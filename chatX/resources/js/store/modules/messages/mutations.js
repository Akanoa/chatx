export const ADD_MESSAGE = 'ADD_MESSAGE';
export const CLEAN_MESSAGES = 'CLEAN_MESSAGES';

export default {
    [ADD_MESSAGE]: (state, message) => {
        if (state.uuids.indexOf(message.uuid) === -1) {
            state.messages.push(message);
            state.uuids.push(message.uuid)
        }
    },
    [CLEAN_MESSAGES]: (state) => {
        Vue.set(state, 'messages', []);
        Vue.set(state, 'uuids', []);
    }
}
