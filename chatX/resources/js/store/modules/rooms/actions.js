import {SET_STEP, STEP_CHOOSE_ROOM, STEP_IN_ROOM} from "../../mutations";
import {SET_CURRENT_ROOM_NAME, SET_CURRENT_ROOM_USERS, SET_CURRENT_ROOM_UUID, SET_ROOMS} from "./mutations";
import {ADD_MESSAGE, CLEAN_MESSAGES} from "../messages/mutations";
import {GET_MESSAGES} from "../messages/actions";

export const ACTION_CREATE_ROOM = 'ACTION_CREATE_ROOM';
export const ACTION_QUIT_ROOM = 'ACTION_QUIT_ROOM';
export const ACTION_ENTER_ROOM = 'ACTION_ENTER_ROOM';
export const GET_ROOMS = 'GET_ROOMS';
export const GET_ROOM_USERS = 'GET_ROOM_USERS';

export default {
    [ACTION_CREATE_ROOM]: ({state, commit, dispatch}, {name}) => {

        fetch('/api/rooms', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name
            })
        }).then(response => {
            if (!response.ok) {
                throw response
            }
            return response.json();
        }).then(data => {
            commit(SET_CURRENT_ROOM_UUID, data);
            commit(SET_CURRENT_ROOM_NAME, data);
            dispatch(ACTION_ENTER_ROOM, {room_uuid: data.uuid})
        })
    },
    [ACTION_QUIT_ROOM]: ({commit, rootState}, ) => {

        let userName = rootState.user.name;
        let userUuid = rootState.user.uuid;

        fetch('/api/users/'+userUuid, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: userName,
                room_uuid: null
            })
        }).then(response => {
            if (!response.ok) {
                throw response
            }
            return response.text();
        }).then(() => {
            commit(SET_CURRENT_ROOM_UUID, {uuid: ""});
            commit(SET_CURRENT_ROOM_NAME, {name: ""});
            commit('messages/'+CLEAN_MESSAGES, null, {root: true});
            commit(SET_STEP, STEP_CHOOSE_ROOM, {root:true});
        })
    },
    [ACTION_ENTER_ROOM]: async ({commit, rootState, dispatch}, {room_uuid}) => {

        let userName = rootState.user.name;
        let userUuid = rootState.user.uuid;

        let rooms = await dispatch(GET_ROOMS);

        let room = rooms.find(r => r.uuid === room_uuid);

        if (!room) {
            return
        }

        fetch('/api/users/' + userUuid, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: userName,
                room_uuid
            })
        }).then(response => {
            if (!response.ok) {
                throw response
            }
            return response.text();
        }).then(() => {

            commit(SET_CURRENT_ROOM_UUID, room);
            commit(SET_CURRENT_ROOM_NAME, room);
            commit(SET_STEP, STEP_IN_ROOM, {root: true});
        })
    },
    [GET_ROOMS]: ({commit}) => {

        return new Promise(resolve => {
            fetch('/api/rooms/', {
                method: 'GET',
            }).then(response => {
                if (!response.ok) {
                    throw response
                }
                return response.json();
            }).then((data) => {
                commit(SET_ROOMS, data)
                resolve(data)
            })
        });
    },
    [GET_ROOM_USERS]: ({commit, rootState}) => {
        let room = rootState.rooms.current_room_uuid;
        fetch('/api/rooms/'+room+'/users')
            .then(response => {
                if (!response.ok) {
                    throw response
                }
                return response.json()
            }).then(data => {
                commit(SET_CURRENT_ROOM_USERS, data)
            })
    }
}
