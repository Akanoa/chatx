import actions from "./actions";
import mutations from "./mutations";

export default {
    state: {
        rooms: [],
        current_room_users: [],
        current_room_uuid: '',
        current_room_name: '',
    },
    actions,
    mutations,
    namespaced: true
}
