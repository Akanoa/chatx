export const SET_ROOMS = 'SET_ROOMS';
export const SET_CURRENT_ROOM_UUID = 'SET_CURRENT_ROOM_UUID';
export const SET_CURRENT_ROOM_NAME = 'SET_CURRENT_ROOM_NAME'
export const SET_CURRENT_ROOM_USERS = 'SET_CURRENT_ROOM_USERS';

export default {
    [SET_ROOMS]: (state, rooms) => {
        Vue.set(state, 'rooms', rooms)
    },
    [SET_CURRENT_ROOM_UUID]: (state, {uuid}) => {
        state.current_room_uuid = uuid
    },
    [SET_CURRENT_ROOM_NAME]: (state, {name}) => {
        state.current_room_name = name
    },
    [SET_CURRENT_ROOM_USERS]: (state, users) => {
        Vue.set(state, 'current_room_users', users)
    }
}
