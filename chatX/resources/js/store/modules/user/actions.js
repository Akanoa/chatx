import {SET_USER_NAME, SET_USER_UUID} from "./mutations";
import {SET_STEP, STEP_CHOOSE_ROOM} from "../../mutations";
import {GET_ROOMS} from "../rooms/actions";

export const ACTION_CREATE_USER = 'ACTION_CREATE_USER';

export default {
    [ACTION_CREATE_USER]: ({state, commit, dispatch}, {name}) => {

        fetch('/api/users', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name
            })
        }).then(response => {
            if (!response.ok) {
                throw response
            }
            return response.json();
        }).then(data => {
            // Mutate the user
            commit(SET_USER_NAME, data);
            commit(SET_USER_UUID, data);
            commit(SET_STEP, STEP_CHOOSE_ROOM, {root:true});
            dispatch('rooms/'+GET_ROOMS, null, {root: true});
        })
    }
}
