import actions from "./actions";
import mutations from "./mutations";

export default {
    state: {
        uuid: "",
        name: ""
    },
    actions,
    mutations,
    namespaced: true
}
