export const SET_USER_UUID = 'SET_USER_UUID';
export const SET_USER_NAME = 'SET_USER_NAME';


export default {
    [SET_USER_UUID]: (state, {uuid}) => {
        state.uuid = uuid
    },
    [SET_USER_NAME]: (state, {name}) => {
        state.name = name
    }
}
