export const TOGGLE_COLLAPSE = 'TOGGLE_COLLAPSE';
export const TOGGLE_ROOM_USERS = 'TOGGLE_ROOM_USERS';
export const SET_STEP = 'SET_STEP';

export const STEP_CONNECTION = 'STEP_CONNECTION';
export const STEP_CHOOSE_ROOM = 'STEP_CHOOSE_ROOM';
export const STEP_CREATE_ROOM = 'STEP_CREATE_ROOM';
export const STEP_IN_ROOM = 'STEP_IN_ROOM';

export default {
    [TOGGLE_COLLAPSE]: state => {
        state.collapse = !state.collapse
    },
    [TOGGLE_ROOM_USERS]: state => {
        state.roomUsers = !state.roomUsers
    },
    [SET_STEP]: (state, step) => {
        state.step = step
    }
}
