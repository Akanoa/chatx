<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <title>ChatX</title>
    </head>
    <body>
    <iframe id="iframe" src="https://vendredi.cc/"></iframe>
    <div id="app">
        <tchat id="tchat"></tchat>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
    @if(config('app.env') == 'local')
        <script src="http://localhost:35729/livereload.js"></script>
    @endif
    </body>
</html>
