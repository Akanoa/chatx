<?php

namespace Test\Unit;

use Tests\TestCase;

class RoomControllerTest extends TestCase
{

    public function testStore()
    {
        $data = [
            'name' => "Algèbre"
        ];

        $this->post(route('rooms.store'), $data)
            ->assertStatus(201)
            ->assertCreated();
    }
}
