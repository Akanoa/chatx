<?php

namespace Test\Unit;

use App\User;
use Tests\TestCase;

class UserControllerTest extends TestCase
{

    public function testStore()
    {
        $data = [
            'name' => "Robert"
        ];

        $this->post(route('users.store'), $data)
            ->assertStatus(201)
            ->assertCreated();
    }

    public function testUpdate()
    {
        $data = [
            'name' => "Robert"
        ];

        $user = $this->post(route('users.store'), $data)->json();

        $data = [
            'name' => "Robert",
            'room_uuid'=> "123"
        ];

        $this->put(route('users.update', $user['uuid']), $data)
            ->assertStatus(200);

        $room_uuid = User::find($user['uuid'])->room_uuid;
        $this->assertEquals("123", $room_uuid);
    }
}
